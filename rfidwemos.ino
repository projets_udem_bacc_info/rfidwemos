/* 69224873 : CARD UID
  *  
  *  Ce fichier va contenir le code qui permet de controler la puce RFID, et son interaction avec la solid state relai
  *  en même temps envoyer les changement au serveur MQTT sur le réseau afin de permettre de controller à distance
  *  Les instructions sur la configuration et l'utilisation de la bibliothèque MFRC522 RFID peut-être trouvées ici : 
  *  
  *  https://github.com/miguelbalboa/rfid/blob/master/README.rst#mfrc522
  *  
  *  l'idée du programme est fortement inspirée d'ici : https://www.youtube.com/watch?v=VSwu-ZYiTxg&t=2335s  mais avec quelques petites différence mineures
  *  
 **/

 
#include <SPI.h>
#include <MFRC522.h>
 
#define RST_PIN     D3                // le port par lequel on va faire un reset sur le lecteur rfid
#define SS_PIN      D8                // le port d'acces au control de notre lecteur rfid
#define LOCK_PIN    D2                // le port par lequel on va interagir avec le relay


String id_puce_enregistre = "69 22 48 73";    // l'id de la puce à vérifier // ceci peut être un tableau ou une base de données dans la quelle on ira lire les puces
String id_puces_deja_lues = "";         // vérifier si la puce à déjà été lue avant de changer son état

MFRC522 mfrc522(SS_PIN, RST_PIN);   // on crée une instance de MFRC522.

enum EtatDeLecture {INIT, ACTIVE, DESACTIVE} ; // Défini si la puce a activé le circuit ou l'a referme

EtatDeLecture etat_de_lecture = INIT; // INITIALISÉ par défaut == fermé

// code d'initialisation de wemosd1 board, du lecteur de la puce
void setup() 
{
  Serial.begin(115200);   // on initialise la connection en série avec le pc
  
  pinMode(LOCK_PIN, OUTPUT);      // on définit le port de sortit qui sera associé au relay
  digitalWrite(LOCK_PIN, HIGH);   // on ferme le circuit du relai
  
  SPI.begin();                    // on Initialise le bus SPI
  delay(100);                     // on s'arrête pour quelques secondes
  
  mfrc522.PCD_Init();             // On initialise le lecteur de notre puce rfid
  Serial.println("En attente de lecture de la carte...");
  Serial.println();
   // info sur la configuration wifi si le mode usb est désactivé
  wifi_setup();
  // configuration du serveur mqtt
  mqtt_setup();
}

// la branche principale de notre programme
void loop() 
{
  String id_puce_lue = "";         // l'id de la puce qu'on vient juste de lire - scanner
  boolean etat_a_change = false;  // lors de la dernière lecture est que l'état de lecture est toujours la même? // a change
  boolean puce_est_reconnue = true; // on suppose que notre puce à au moins été lue par le lecteur et qu'elle est reconnue

  mqtt_loop();
  
  // si le lecteur est en mésure de lire la carte
  if ( mfrc522.PICC_IsNewCardPresent()) 
  {
     //Serial.println(F("Puce Présente")); // on pourrait afficher les détails de lecture pour débogage
    if ( mfrc522.PICC_ReadCardSerial())   // on va selectionner la carte présente au lecteur pour soutirer des informations
    {
      //Serial.println(F("Puce Lue avant verification"));      // si la lecture s'est faite avec succès
      id_puce_lue = extraire_id_puce(mfrc522.uid.uidByte, mfrc522.uid.size );                    // on extrait l'id de la puce en format voulu
    }
    else  // si non on recommence le processus de lecture
    {
      return;
    }
  }
  else // si non on dispose le lecture à la prochaine lecture
  {
    return;
  }
  // on vérifie si la puce avait déja été lu au paravant
  // ici on veut garder l'état de notre puces qu'il ait changé ou pas, puisque le scan au lecteur joue juste unn role d'interrupteur
  // on pourrait améliorer le scénario autrement
  if(id_puce_lue != id_puces_deja_lues)
  {
    etat_a_change = true;
    id_puces_deja_lues = id_puce_lue;   // on ajoute la puce qu'on vient de lire à la liste des puces déjà lues : effectif dans un tableau si on dispose de plusieur puces
  }
  // si la puce n'est pas celle que l'on veut lire ou ne fait pas partie des puces autorisée
  if(id_puces_deja_lues != id_puce_enregistre)
  {
    puce_est_reconnue = false;
  }
   // on arrête la lecture
   mfrc522.PICC_HaltA();
   Serial.println(F("Lecture arrêtée"));
   // on arrêtre le systeme de cryptage de la carte
   mfrc522.PCD_StopCrypto1();
   Serial.println(F("crypto arrêté"));
   
  // ensuite nous allons en fonction de l'état de lecture de notre puce publier les messages au serveur mqtt
  switch(etat_de_lecture)
  {
    case INIT :
      etat_de_lecture = ACTIVE; // si c'est état d'initialisation, on ouvre le relai
      break;
    case ACTIVE :
      if(puce_est_reconnue){
        enModeActif();         // on active le relai en mode ouvert == on ouvre l'interrupteur
      }
      break;
    case DESACTIVE :
      if(puce_est_reconnue){
        enModeDesactif();         // on active le relai en mode fermé == on ferme l'interrupteur
      }
      break;
  }
  // on appelle la fonction mqtt pour signaler au serveur
 
  // on fait une tite pause, avant la prochaine action(lecture)
  delay(100);
}
// action qui se passe lorsque la puce est lue par le lecteur et qu'elle est reconnue
void enModeActif()
{
  // on affiche le message
  Serial.println(F("Puce ouverte et verifie"));
  digitalWrite(LOCK_PIN, LOW);      // on ouvre l'interrupteur
  // On publie le message sur le serveur MQTT
  //publish("PUCEENLECTURE");
  publish("La puce RFID a active le relai");
  // on met à jours les info sur la lecture de la puce
  etat_de_lecture = DESACTIVE;
  // on fait une tite pause, avant la prochaine action(lecture)
  delay(2000);
}
// mode lu
void enModeDesactif()
{
  // on affiche le message
  Serial.println(F("Puce ferme et verifie"));
  digitalWrite(LOCK_PIN, HIGH);      // on ouvre l'interrupteur
  // On publie le message sur le serveur MQTT
  publish("La puce RFID a desactive le relai");
  // on met à jours les info sur la lecture de la puce
  etat_de_lecture = INIT;
  // on fait une tite pause, avant la prochaine action(lecture)
  delay(2000);
}
// en mode réinitialisation
void enModeInit()
{
  // on affiche le message
  Serial.println(F("Puce Lecture initialisation"));
  // on effectue les changements sur notre relai(on le relache)
  digitalWrite(LOCK_PIN, HIGH);

  // On publie le message sur le serveur MQTT
  publish("La puce RFID a reinitialise le relai");
  // on met à jours les info sur la lecture de la puce // ON RECOMMENCE LA LECTURE(change d'état)
  etat_de_lecture = INIT;

  // on fait une tite pause, avant la prochaine action(lecture)
  delay(2000);
}
// Méthode qui va nous permettre d'extraire l'id de la puce et de le formatter au format voulu
String extraire_id_puce(byte *buffer, byte bufferSize)
{
  String id_temp = "";
  for (byte i = 0; i < bufferSize; i++) 
  {
     id_temp.concat(String(buffer[i] < 0x10 ? " 0" : " "));
     id_temp.concat(String(buffer[i], HEX));              // formattage de la chaine lue
  }
  id_temp.toUpperCase();                                  // on va mettre tous les caractères en majuscule

  return id_temp.substring(1);                            // on retourne l'id de la carte en format voulu
}
