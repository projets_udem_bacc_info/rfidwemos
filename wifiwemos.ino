/* 
  *  Ce fichier va contenir le code qui permet d'utiliser les fonctionalité wifi de notre wemos board et de la connecter au serveur MQTT
  *  en même temps envoyer les changement au serveur MQTT sur le réseau afin de permettre de controller à distance
  *  On va utilser la bibiotheque : ESP8266 pour les cartes wimos et la bibliothèque publish subscribe mqtt
  *  Les détails sur la facon d'utiliser ces bibilothèques peuvent se trouver en ligne sur le site github des concepteurs respectifs
  *  
  *  pour la biblio wifi ESP8266 les détails se trouvent ici : https://github.com/esp8266/Arduino/blob/master/README.md#installing-with-boards-manager
  *  Et la biblio MQTT ici : https://github.com/knolleary/pubsubclient/blob/master/README.md
  *  
  *  l'idée du programme est fortement inspirée d'ici : https://www.youtube.com/watch?v=VSwu-ZYiTxg&t=2335s mais avec quelques petites différence mineures
 **/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Définition des information concernant notre connection au wifi
#define WIFI_ID   "TEST"       //"BELL133" 139.103.64.13 
#define WIFI_PWD  "1234567890" //"2CCEE5E3EDDA"
#define ID_CARTE  "WEMOS_D1"

IPAddress ip_serveur_mqtt(192, 168, 2, 11);   // on configure la connection au serveur MQTT : 192.168.2.11 AT HOME

WiFiClient    client_wifi;                       // on crée une instance du client wifi qui se connecte au serveur
PubSubClient  client_mqtt(client_wifi);           // instance du client mqtt sur le client wifi

char message[64];                             // taille maximale que peut prendre un message envoyé ou recu
char sujet[32];                               // sujet auquel les clients se connecte : dans mqtt on publie souvent des sujets(topic)!!

// fonction de rappelle chaque fois qu'un message est publié sur n'importe quel sujet ce client est souscrit
// contenue ici n'est que ce que contient le sujet envoyé par le serveur ou du client vers le serveur
void callback(char* topic, byte* payload, unsigned int length )
{
  // on copie et on converti en chaine de caractère le contenu renvoyé du serveur 
  memcpy(message, payload, length);
  // on va ajouter un caractère nulle à la fin de notre chaine(type C)
  message[length] = '\0';

  //affichage du message recu pour débogage
  Serial.print("Message recu dans le sujet : [");
  Serial.print(topic);
  Serial.print("]");
  Serial.println(message);

  // actions prise en fonction du contenu du message
   if(strcmp(message, "ACTIVE") == 0){
    enModeActif();
  }
  else if(strcmp(message, "DESACTIVE") == 0){
    enModeInit();
  }
}
// fonction pour la configuration du wifi
void wifi_setup()
{
  //if(!Serial){
    //Serial.begin(9600);
  //}
  // tentative de connection au réseau wifi
  delay(10); // une tite pause avant de commencer
  Serial.print("Tentative de connection au réseau : ");
  Serial.println(WIFI_ID);
  WiFi.begin(WIFI_ID, WIFI_PWD); 

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  delay(2000);
  Serial.println("");
  Serial.println("WiFi connecté");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}
// configuration de la methode mqtt
void mqtt_setup()
{
  // on définit les configuration liée au serveur MQTT
  client_mqtt.setServer(ip_serveur_mqtt, 1883);
  client_mqtt.setCallback(callback);
}
// la boucle principale de MQTT
void mqtt_loop()
{
  // on fait sur qu'il y a une connection etablie au seveur mqtt
  while(!client_mqtt.connected())
  {
    // on affiche si la connection se passe bien au serveur
    Serial.print("Tentative de connection au serveur MQTT a l'adresse : ");
    Serial.println(ip_serveur_mqtt);
    // on essai de connecter la carte wemos
    if(client_mqtt.connect(ID_CARTE))
    {
      // si tout se passe bien
      Serial.println("Connection reussie au serveur MQTT");
      //client_mqtt.setCallback(callback);
      snprintf(sujet, 32, "ToHost/%s", ID_CARTE);
      //client_mqtt.subscribe(sujet);
      // on publie un message si la connection s'est faite avec succès
      //snprintf(sujet, 32, "ToHost/%s", ID_CARTE);
      snprintf(message, 64, "CONNECT %s", ID_CARTE);
      client_mqtt.publish(sujet, message);

      // on souscrit aux sujets concernant notre carte wemos d1
      snprintf(sujet, 32, "ToDevice/%s", ID_CARTE);
      client_mqtt.subscribe(sujet);
      // on souscrit à tous les sujets
      client_mqtt.subscribe("ToDevice/All");
    }
    else // si la connection a echoué on affiche les détails
    {
      Serial.print("La connection au serveur a echoue : ");
      Serial.print(client_mqtt.state());
      Serial.println(" tentative de reconnection dans 5 secondes");
      //on attends 5 secondes avant de recommencer
      delay(5000);
    }
  }
  //après on appelle à la boucle principale de la libraire mqtt pour chercher et souscrire aux différents message
  client_mqtt.loop();
  //delay(2000);
}
// fonction qui va nous permettre de publier un message si la carte rfid est activé ou pas
void publish(char* message)
{
  snprintf(sujet, 32, "ToHost/%s", ID_CARTE);
  client_mqtt.publish(sujet, message);
}
 
